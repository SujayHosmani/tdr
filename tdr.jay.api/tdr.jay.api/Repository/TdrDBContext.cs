﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdr.jay.api.Entities;
using tdr.jay.api.Repository.EntityConfiguration;

namespace tdr.jay.api.Repository
{
    public class TdrDBContext : DbContext
    {
        public TdrDBContext(DbContextOptions<TdrDBContext> dbContext) : base(dbContext)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserLoginConfiguration());
            builder.ApplyConfiguration(new TaskDescConfiguration());
            builder.ApplyConfiguration(new ImgConfiguration());
            builder.ApplyConfiguration(new TaskConfiguration());
           // builder.ApplyConfiguration(new MessegeConfiguration());
            builder.ApplyConfiguration(new WorkFlowConfiguration());
        }

       public DbSet<UserRegistration> UserRegistraion { get; set; }
       public DbSet<TaskDesc> TaskDescs { get; set; }
       public DbSet<Image> images { get; set; }
       public DbSet<Tasks> tasks { get; set; }
        public DbSet<Message> messages { get; set; }
       public DbSet<WorkFlow> workFlows { get; set; }
    }
}
