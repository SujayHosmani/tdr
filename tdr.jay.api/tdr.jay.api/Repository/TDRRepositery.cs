﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdr.jay.api.Entities;

namespace tdr.jay.api.Repository
{
    public class TDRRepositery
    {
        public TDRRepositery(TdrDBContext tdrDBContext)
        {
            TdrDBContext = tdrDBContext;
        }

        public TdrDBContext TdrDBContext { get; }

        public async Task<bool> AddUser(UserRegistration userRegistration)
        {
            await TdrDBContext.UserRegistraion.AddAsync(userRegistration);

            return await TdrDBContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> VerifyUser(string UserId)
        {
            return await TdrDBContext.UserRegistraion.Where(a => a.UserId == UserId).AnyAsync();

        }

        public async Task<UserRegistration> GetUser(string UserId)
        {
            return await TdrDBContext.UserRegistraion.Where(a => a.UserId == UserId).FirstOrDefaultAsync();

        }

        public async Task<Tasks> GetTaskById(long taskId)
        {
            return await TdrDBContext.tasks.Where(a => a.TaskId == taskId).FirstOrDefaultAsync();

        }
        public async Task<List<UserRegistration>> GetUsers()
        {
            return await TdrDBContext.UserRegistraion.ToListAsync();

        }
        public async Task<TaskCreated> SetTaskDesc(TaskDesc taskDesc)
        {
            var taskdesc = await TdrDBContext.TaskDescs.AddAsync(taskDesc);

            await TdrDBContext.SaveChangesAsync();

            return taskdesc != null ? new TaskCreated
            {
                Id = taskDesc?.TaskId.ToString(),

                CreatedDt = taskDesc?.Date.ToString(),

                CreatedUser = taskDesc?.TaskBy
            } : null;
        }

        public async Task<List<TaskDesc>> GetTaskDescs()
        {
            return await TdrDBContext.TaskDescs.OrderByDescending(a => a.TaskId).ToListAsync();
        }

        public async Task<List<TaskDesc>> GetTaskDescsBy(string taskBy)
        {
            return await TdrDBContext.TaskDescs.Where(j => j.TaskBy == taskBy).ToListAsync();
        }

        public async Task<List<TaskDesc>> GetTaskDescsTo(string taskTo)
        {
            return await TdrDBContext.TaskDescs.Where(j => j.TaskTo == taskTo).ToListAsync();
        }

        public async Task<int> UpdateTaskDescs(TaskDesc taskDesc)
        {
            TdrDBContext.Entry(await TdrDBContext.TaskDescs.FirstOrDefaultAsync(x => x.TaskId == taskDesc.TaskId)).CurrentValues.SetValues(taskDesc);

            return await TdrDBContext.SaveChangesAsync();
        }
        public async Task<int> SaveImage(Image image)
        {
            await TdrDBContext.images.AddAsync(image);

            await TdrDBContext.SaveChangesAsync();

            Task.WaitAll();

            return image.Id;
        }
        public async Task<List<Image>> GetImagesAsync(int taskId)
        {
            return await TdrDBContext.images.Where(e => e.TaskId == taskId.ToString()).ToListAsync();
        }
        public async Task<int> SaveImgTaskId(int id, long taskId)
        {
            var taskid = await TdrDBContext.images.Where(e => e.Id == id).FirstOrDefaultAsync();

            if (taskid != null)
            {
                taskid.TaskId = taskId.ToString();
            }

            return await TdrDBContext.SaveChangesAsync();
        }
        public async Task<TaskCreated> SetTaskDescV101(Tasks taskDesc)
        {
            var taskdesc = await TdrDBContext.tasks.AddAsync(taskDesc);

            await TdrDBContext.SaveChangesAsync();

            return taskdesc != null ? new TaskCreated
            {
                Id = taskDesc?.TaskId.ToString(),

                CreatedDt = taskDesc?.CreatedDt.ToString(),

                CreatedUser = taskDesc?.TaskByName
            } : null;
        }

        public async Task<List<Tasks>> GetTaskDescsV101()
        {
            return await TdrDBContext.tasks.OrderByDescending(a => a.TaskId).ToListAsync();
        }

        public async Task<PaginationModel> GetTasksByPaginationFitersV01(TasksFilter taskDescFilter, int pageIndex, int pageSize, string from)
        {
            PaginationModel paginationMod = new PaginationModel();
            if (from == "all" || from == "open")
            {
                var openTasks = TdrDBContext.tasks.OrderByDescending(a => a.ModifiedDt).Where(a => a.Status == "open")
                            .Where(a => a.TaskType == null || a.TaskType == a.TaskType)
                            .Where(t => taskDescFilter.TaskType == null || taskDescFilter.TaskType == t.TaskType)
                            .Where(t => taskDescFilter.Status == null || taskDescFilter.Status == t.Status)
                            .Where(t => taskDescFilter.Platform == null || taskDescFilter.Platform == t.Platform)
                            .Where(t => taskDescFilter.DevStatus == null || taskDescFilter.DevStatus == t.DevStatus)
                            .Where(t => taskDescFilter.TestStatus == null || taskDescFilter.TestStatus == t.TestStatus)
                            .Where(t => taskDescFilter.DevId == null || taskDescFilter.DevId == t.DevId)
                            .Where(t => taskDescFilter.TaskById == null || taskDescFilter.TaskById == t.TaskById)
                            .Where(t => taskDescFilter.TaskToId == null || (taskDescFilter.TaskToId == t.TaskToId ||
                                   t.TaskToId == taskDescFilter.Team))
                            .Where(t => taskDescFilter.Priority == null || taskDescFilter.Priority == t.Priority)
                            .Where(t => taskDescFilter.Started == null || taskDescFilter.Started == t.Started)
                            .Where(t => taskDescFilter.Reopened == null || taskDescFilter.Reopened == t.ReOpened)
                            .Where(t => taskDescFilter.CreatedDt == null || taskDescFilter.CreatedDt == t.CreatedDt.ToString("dd/MM/yyyy"))
                            .Where(t => taskDescFilter.ModifiedDt == null || taskDescFilter.ModifiedDt == t.ModifiedDt.ToString("dd/MM/yyyy"));

                            paginationMod.Open = await openTasks.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
                            paginationMod.OpenCount = openTasks.Count();
            }

            if (from == "all" || from == "progress")
            {
                var progressTasks = TdrDBContext.tasks.OrderByDescending(a => a.ModifiedDt).Where(a => a.Status == "progress")
                            .Where(a => a.TaskType == null || a.TaskType == a.TaskType)
                            .Where(t => taskDescFilter.TaskType == null || taskDescFilter.TaskType == t.TaskType)
                            .Where(t => taskDescFilter.Status == null || taskDescFilter.Status == t.Status)
                            .Where(t => taskDescFilter.Platform == null || taskDescFilter.Platform == t.Platform)
                            .Where(t => taskDescFilter.DevStatus == null || taskDescFilter.DevStatus == t.DevStatus)
                            .Where(t => taskDescFilter.TestStatus == null || taskDescFilter.TestStatus == t.TestStatus)
                            .Where(t => taskDescFilter.DevId == null || taskDescFilter.DevId == t.DevId)
                            .Where(t => taskDescFilter.TaskById == null || taskDescFilter.TaskById == t.TaskById)
                            .Where(t => taskDescFilter.TaskToId == null || (taskDescFilter.TaskToId == t.TaskToId ||
                                   t.TaskToId == taskDescFilter.Team))
                            .Where(t => taskDescFilter.Priority == null || taskDescFilter.Priority == t.Priority)
                            .Where(t => taskDescFilter.Started == null || taskDescFilter.Started == t.Started)
                            .Where(t => taskDescFilter.Reopened == null || taskDescFilter.Reopened == t.ReOpened)
                            .Where(t => taskDescFilter.CreatedDt == null || taskDescFilter.CreatedDt == t.CreatedDt.ToString("dd/MM/yyyy"))
                            .Where(t => taskDescFilter.ModifiedDt == null || taskDescFilter.ModifiedDt == t.ModifiedDt.ToString("dd/MM/yyyy"));

                            paginationMod.Progress = await progressTasks.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();;
                            paginationMod.ProgressCount = progressTasks.Count();
            }

            if (from == "all" || from == "resolved")
            {
                var resolvedTasks = TdrDBContext.tasks.OrderByDescending(a => a.ModifiedDt).Where(a => a.Status == "resolved")
                            .Where(a => a.TaskType == null || a.TaskType == a.TaskType)
                            .Where(t => taskDescFilter.TaskType == null || taskDescFilter.TaskType == t.TaskType)
                            .Where(t => taskDescFilter.Status == null || taskDescFilter.Status == t.Status)
                            .Where(t => taskDescFilter.Platform == null || taskDescFilter.Platform == t.Platform)
                            .Where(t => taskDescFilter.DevStatus == null || taskDescFilter.DevStatus == t.DevStatus)
                            .Where(t => taskDescFilter.TestStatus == null || taskDescFilter.TestStatus == t.TestStatus)
                            .Where(t => taskDescFilter.DevId == null || taskDescFilter.DevId == t.DevId)
                            .Where(t => taskDescFilter.TaskById == null || taskDescFilter.TaskById == t.TaskById)
                            .Where(t => taskDescFilter.TaskToId == null || (taskDescFilter.TaskToId == t.TaskToId ||
                                   t.TaskToId == taskDescFilter.Team))
                            .Where(t => taskDescFilter.Priority == null || taskDescFilter.Priority == t.Priority)
                            .Where(t => taskDescFilter.Started == null || taskDescFilter.Started == t.Started)
                            .Where(t => taskDescFilter.Reopened == null || taskDescFilter.Reopened == t.ReOpened)
                            .Where(t => taskDescFilter.CreatedDt == null || taskDescFilter.CreatedDt == t.CreatedDt.ToString("dd/MM/yyyy"))
                            .Where(t => taskDescFilter.ModifiedDt == null || taskDescFilter.ModifiedDt == t.ModifiedDt.ToString("dd/MM/yyyy"));
                            

                            paginationMod.Resolved = await resolvedTasks.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();;
                            paginationMod.ResolvedCount = resolvedTasks.Count();
            }

            if (from == "all" || from == "closed")
            {
                var closedTasks = TdrDBContext.tasks.OrderByDescending(a => a.ModifiedDt).Where(a => a.Status == "closed")
                            .Where(a => a.TaskType == null || a.TaskType == a.TaskType)
                            .Where(t => taskDescFilter.TaskType == null || taskDescFilter.TaskType == t.TaskType)
                            .Where(t => taskDescFilter.Status == null || taskDescFilter.Status == t.Status)
                            .Where(t => taskDescFilter.Platform == null || taskDescFilter.Platform == t.Platform)
                            .Where(t => taskDescFilter.DevStatus == null || taskDescFilter.DevStatus == t.DevStatus)
                            .Where(t => taskDescFilter.TestStatus == null || taskDescFilter.TestStatus == t.TestStatus)
                            .Where(t => taskDescFilter.DevId == null || taskDescFilter.DevId == t.DevId)
                            .Where(t => taskDescFilter.TaskById == null || taskDescFilter.TaskById == t.TaskById)
                            .Where(t => taskDescFilter.TaskToId == null || (taskDescFilter.TaskToId == t.TaskToId ||
                                   t.TaskToId == taskDescFilter.Team))
                            .Where(t => taskDescFilter.Priority == null || taskDescFilter.Priority == t.Priority)
                            .Where(t => taskDescFilter.Started == null || taskDescFilter.Started == t.Started)
                            .Where(t => taskDescFilter.Reopened == null || taskDescFilter.Reopened == t.ReOpened)
                            .Where(t => taskDescFilter.CreatedDt == null || taskDescFilter.CreatedDt == t.CreatedDt.ToString("dd/MM/yyyy"))
                            .Where(t => taskDescFilter.ModifiedDt == null || taskDescFilter.ModifiedDt == t.ModifiedDt.ToString("dd/MM/yyyy"));
                            

                            paginationMod.Closed = await closedTasks.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();;
                            paginationMod.ClosedCount = closedTasks.Count();
            }

            


            return paginationMod;

        }
        public async Task<int> UpdateTaskDescsV101(Tasks taskDesc)
        {
            TdrDBContext.Entry(await TdrDBContext.tasks.FirstOrDefaultAsync(x => x.TaskId == taskDesc.TaskId)).CurrentValues.SetValues(taskDesc);

            return await TdrDBContext.SaveChangesAsync();
        }

        public async Task<CountModel> GetTaskCountV01()
        {
            CountModel countModel = new CountModel();

            countModel.Closed = await TdrDBContext.tasks.Where(j => j.Status == "closed").CountAsync();

            countModel.Open = await TdrDBContext.tasks.Where(j => j.Status == "open").CountAsync();

            countModel.Resolved = await TdrDBContext.tasks.Where(j => j.Status == "resolved").CountAsync();

            countModel.Reopened = await TdrDBContext.tasks.Where(j => j.Status == "reopened").CountAsync();

            countModel.Progress = await TdrDBContext.tasks.Where(j => j.Status == "progress").CountAsync();

            countModel.Total = await TdrDBContext.tasks.CountAsync();

            return countModel;
        }

        public async Task<int> SetWorkFlow(WorkFlow workFlow)
        {
            await TdrDBContext.workFlows.AddAsync(workFlow);

            return await TdrDBContext.SaveChangesAsync();
        }

        public async Task<int> UpdateWorkFlow(WorkFlow workFlow)
        {
            TdrDBContext.Entry(await TdrDBContext.workFlows.FirstOrDefaultAsync(x => x.TaskId == workFlow.TaskId)).CurrentValues.SetValues(workFlow);

            return await TdrDBContext.SaveChangesAsync();
        }

        public async Task<List<WorkFlow>> GetWorkFlowAsync(int id)
        {
            return await TdrDBContext.workFlows.Where(j => j.TaskId == id).OrderByDescending(j => j.Id).ToListAsync();
            //OrderByDescending(j => j.TaskId)
        }

        public async Task<List<WorkFlow>> GetWorkFlowAsync()
        {
            return await TdrDBContext.workFlows.Where(e => e.TaskId != -999999999).OrderByDescending(j => j.TaskId).ToListAsync();

        }


    }
}
