﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdr.jay.api.Bussiness.Response;
using tdr.jay.api.Entities;
using tdr.jay.api.Repository.EntityConfiguration;

namespace tdr.jay.api.Repository.Contracts
{
    public interface ITDRService 
    {
        public Task<TDRResponse<string>> UserRegigstraion(UserRegistration userRegistration);
        public Task<TDRResponse<UserDetail>> VerifyUser(Login login);
        public Task<TDRResponse<List<UserDetail>>> GetUserDetails();
        public Task<TDRResponse<UserDetail>> GetUserDetail(string user);
        public Task<TDRResponse<Tasks>> GetTaskById(long taskId);
        public Task<TDRResponse<TaskCreated>> TaskDescription(TaskDesc taskDesc);
        public Task<TDRResponse<List<TaskDesc>>> GetTaskDescriptions();
        public Task<TDRResponse<List<TaskDesc>>> GetTaskDescriptionsByTaskBy(string taskBy);
        public Task<TDRResponse<List<TaskDesc>>> GetTaskDescriptionsByTaskTo(string taskto);
        public Task<TDRResponse<List<TaskDesc>>> GetTaskDescriptionByFiters(TaskDescFilter taskDescFilter);
        public Task<TDRResponse<string>> UpdateTaskDescriptions(TaskDesc taskDesc);
        public Task<TDRResponse<int>> SaveImage(string Images, string type);
        public Task<TDRResponse<List<Image>>> GetImage(int taskId);
        public Task<TDRResponse<TaskCreated>> TaskDescriptionV101(Tasks taskDesc);
        public Task<TDRResponse<List<Tasks>>> GetTaskDescriptionsV101();
        public Task<TDRResponse<List<Tasks>>> GetTaskDescriptionByFitersV101(TasksFilter taskDescFilter,int pageIndex, int pageSize);
        public Task<TDRResponse<PaginationModel>> GetTasksByPaginationFitersV01(TasksFilter taskDescFilter,int pageIndex, int pageSize, string from);
        public Task<TDRResponse<string>> UpdateTaskDescriptionsV101(Tasks taskDesc);
        public Task<TDRResponse<CountModel>> GetTaskCountV01();
        public Task<TDRResponse<List<WorkFlow>>> GetWorkFlowById(int id);
        public Task<TDRResponse<List<WorkFlow>>> GetWorkFlows();
        public Task<TDRResponse<List<WorkFlow>>> GetWorkFlowsByPage(int pageIndex, int pageSize);
    }
}
