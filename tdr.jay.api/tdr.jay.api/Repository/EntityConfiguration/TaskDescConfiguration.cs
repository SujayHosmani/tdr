﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdr.jay.api.Entities;

namespace tdr.jay.api.Repository.EntityConfiguration
{
    public class TaskDescConfiguration : IEntityTypeConfiguration<TaskDesc>
    {
        public void Configure(EntityTypeBuilder<TaskDesc> builder)
        {
            builder.ToTable("tdr_task").
                HasKey(j => j.TaskId);

            builder.Property(j => j.TaskId).
                HasColumnName("task_id");

			builder.Property(j => j.TaskTitle).
			   HasColumnName("task_title");

			builder.Property(j => j.TaskDescription).
			   HasColumnName("task_description");

			builder.Property(j => j.Images).
			   HasColumnName("images");

			builder.Property(j => j.Platform).
			   HasColumnName("platform");

			builder.Property(j => j.TaskBy).
			   HasColumnName("task_by");

			builder.Property(j => j.TaskTo).
			   HasColumnName("task_to");

			builder.Property(j => j.Date).
			   HasColumnName("date");

			builder.Property(j => j.Priority).
			   HasColumnName("priority");

			builder.Property(j => j.Accepted).
			   HasColumnName("accepted");

			builder.Property(j => j.Rejected).
			   HasColumnName("rejected");

			builder.Property(j => j.Solved).
			   HasColumnName("solved");

			builder.Property(j => j.DevRemark).
			   HasColumnName("dev_remark");

			builder.Property(j => j.TesterRemark).
			   HasColumnName("tester_remark");

			builder.Property(j => j.Started).
			   HasColumnName("started");

			builder.Property(j => j.Resolved).
			   HasColumnName("resolved");

			builder.Property(j => j.ReIssueId).
			   HasColumnName("re_issue_id");

			builder.Property(j => j.ReIssued).
			   HasColumnName("re_issued");

			builder.Property(j => j.From).
			   HasColumnName("from");

			builder.Property(j => j.To).
			   HasColumnName("to");

			builder.Property(j => j.UnSolved).
			   HasColumnName("un_solved");

			builder.Property(j => j.UnResolved).
			   HasColumnName("un_resolved");

			builder.Property(j => j.DevName).
			   HasColumnName("dev_name");

			builder.Property(j => j.DevId).
			   HasColumnName("dev_id");

			builder.Property(j => j.ProjectName).
			   HasColumnName("project_name");

		   

		}
    }
}



	
	


	 

	  


	   

	   
	
	
	
	


	

	
	
	
	
	