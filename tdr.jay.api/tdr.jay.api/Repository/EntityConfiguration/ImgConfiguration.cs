﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdr.jay.api.Entities;

namespace tdr.jay.api.Repository.EntityConfiguration
{
    public class ImgConfiguration : IEntityTypeConfiguration<Image>
    {
        public void Configure(EntityTypeBuilder<Image> builder)
        {
            builder.ToTable("images").
                HasKey(j => j.Id);

            builder.Property(j => j.Id).
                HasColumnName("id");

            builder.Property(j => j.Img).
                HasColumnName("image");

            builder.Property(j => j.TaskId).
               HasColumnName("task_id");

            builder.Property(j => j.Type).
               HasColumnName("type");

        }
    }
}
