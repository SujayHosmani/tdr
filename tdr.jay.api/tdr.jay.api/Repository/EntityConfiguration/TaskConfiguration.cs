﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdr.jay.api.Entities;

namespace tdr.jay.api.Repository.EntityConfiguration
{
    public class TaskConfiguration : IEntityTypeConfiguration<Tasks>
    {
        public void Configure(EntityTypeBuilder<Tasks> builder)
        {
			builder.ToTable("task").
			   HasKey(j => j.TaskId);

			builder.Property(j => j.TaskId).
				HasColumnName("task_id");

			builder.Property(j => j.TaskType).
			   HasColumnName("task_type");

			builder.Property(j => j.TaskSub).
			   HasColumnName("task_subject");

			builder.Property(j => j.Status).
			   HasColumnName("task_status");

			builder.Property(j => j.DevStatus).
			   HasColumnName("dev_status");

			builder.Property(j => j.TestStatus).
			   HasColumnName("test_status");

			builder.Property(j => j.ImgCount).
			   HasColumnName("image_count");

			builder.Property(j => j.TaskDesc).
			   HasColumnName("task_desc");

			builder.Property(j => j.DevName).
			   HasColumnName("dev_name");

			builder.Property(j => j.DevId).
			   HasColumnName("dev_id");

			builder.Property(j => j.Platform).
			   HasColumnName("platform");

			builder.Property(j => j.DevRemark).
			   HasColumnName("dev_remark");

			builder.Property(j => j.TestRemark).
			   HasColumnName("test_remark");

			builder.Property(j => j.TaskById).
			   HasColumnName("task_by_id");

			builder.Property(j => j.TaskToId).
			   HasColumnName("task_to_id");

			builder.Property(j => j.TaskByName).
			   HasColumnName("task_by_name");

			builder.Property(j => j.TaskToName).
			   HasColumnName("task_to_name");

			builder.Property(j => j.Priority).
			   HasColumnName("priority");

			builder.Property(j => j.CreatedDt).
			   HasColumnName("created_date");

			builder.Property(j => j.ModifiedDt).
			   HasColumnName("modified_date");

			builder.Property(j => j.ProjName).
			   HasColumnName("proj_name");

			builder.Property(j => j.Started).
			   HasColumnName("started");

			builder.Property(j => j.ReOpened).
			   HasColumnName("reopened");

			builder.Property(j => j.ReOpenedId).
			   HasColumnName("reopened_id");
		}
    }
}
