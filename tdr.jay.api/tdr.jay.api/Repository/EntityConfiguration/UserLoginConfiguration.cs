﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdr.jay.api.Entities;

namespace tdr.jay.api.Repository.EntityConfiguration
{
    public class UserLoginConfiguration : IEntityTypeConfiguration<UserRegistration>
    {
        public void Configure(EntityTypeBuilder<UserRegistration> builder)
        {
            builder.ToTable("tdr_user").
                  HasKey(a => a.UniqNo);

            builder.Property(a => a.UniqNo).
                HasColumnName("uniq_no");

            builder.Property(a => a.UserId).
                HasColumnName("user_id");

            builder.Property(a => a.UserName).
               HasColumnName("user_name");

            builder.Property(a => a.Password).
                HasColumnName("user_password");

            builder.Property(a => a.UserDesignation).
               HasColumnName("user_designation");

        }
    }
}
