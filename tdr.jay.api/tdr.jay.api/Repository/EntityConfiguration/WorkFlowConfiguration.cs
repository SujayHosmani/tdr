﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdr.jay.api.Entities;

namespace tdr.jay.api.Repository.EntityConfiguration
{
    public class WorkFlowConfiguration : IEntityTypeConfiguration<WorkFlow>
    {
        public void Configure(EntityTypeBuilder<WorkFlow> builder)
        {
            builder.ToTable("work_flow").
                 HasKey(j => j.Id);

            builder.Property(j => j.Id).
                HasColumnName("id");

            builder.Property(j => j.TaskId).
                HasColumnName("task_id");

            builder.Property(j => j.TaskType).
               HasColumnName("task_type");

            builder.Property(j => j.TaskSub).
               HasColumnName("task_subject");

            builder.Property(j => j.Status).
               HasColumnName("task_status");

            builder.Property(j => j.DevStatus).
               HasColumnName("dev_status");

            builder.Property(j => j.TestStatus).
               HasColumnName("test_status");

            builder.Property(j => j.TaskDesc).
               HasColumnName("task_desc");

            builder.Property(j => j.Platform).
               HasColumnName("platform");

            builder.Property(j => j.DevRemark).
               HasColumnName("dev_remark");

            builder.Property(j => j.TestRemark).
               HasColumnName("test_remark");

            builder.Property(j => j.TaskById).
               HasColumnName("task_by_id");

            builder.Property(j => j.TaskToId).
               HasColumnName("task_to_id");

            builder.Property(j => j.TaskByName).
               HasColumnName("task_by_name");

            builder.Property(j => j.TaskToName).
               HasColumnName("task_to_name");

            builder.Property(j => j.Priority).
               HasColumnName("priority");

            builder.Property(j => j.CreatedDt).
               HasColumnName("created_date");

            builder.Property(j => j.From).
               HasColumnName("from");

            builder.Property(j => j.ProjName).
               HasColumnName("proj_name");

            builder.Property(j => j.To).
               HasColumnName("to");

            builder.Property(j => j.Flow).
               HasColumnName("flow");

            builder.Property(j => j.Role).
               HasColumnName("role");

            builder.Property(j => j.DevName).
            HasColumnName("dev_name");

            builder.Property(j => j.DevId).
               HasColumnName("dev_id");

            builder.Property(j => j.Movedby).
            HasColumnName("moved_by");

            builder.Property(j => j.MovedbyId).
               HasColumnName("moved_by_id");
        }
    }
}
