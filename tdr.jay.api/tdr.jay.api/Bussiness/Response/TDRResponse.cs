﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdr.jay.api.Bussiness.Response
{
    public class TDRResponse<T>
    { 
        //first
        public TDRResponse()
        {

        }
        public int Status { get; set; }
        public T Data { get; set; }
        public string Error { get; set; }

        public TDRResponse(int status, T data , string errorDetail)
        {
            this.Status = status;

            this.Data = data;

            this.Error = errorDetail;
        }
    }

    public class ErrorDetail
    {
        public string ErrorDesc { get; set; }
        public string ErrorCode { get; set; }
    }
}
