﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdr.jay.api.Bussiness.Response;
using tdr.jay.api.Entities;
using tdr.jay.api.Repository;
using System.Globalization;
using System.Threading;
using tdr.jay.api.Repository.Contracts;

namespace tdr.jay.api.Bussiness
{
    public class TDRBussiness : ITDRService
    {
        public TDRBussiness(TDRRepositery tDRRepositery)
        {
            TDRRepositery = tDRRepositery;
        }

        public TDRRepositery TDRRepositery { get; }

        public async Task<TDRResponse<UserDetail>> VerifyUser(Login login)
        {
            UserRegistration userRegistration = await TDRRepositery.GetUser(login.UserId);

            if (userRegistration != null)
            {
                bool isUser = userRegistration.Password.Equals(login.Password, StringComparison.InvariantCultureIgnoreCase);

                if (isUser)
                {
                    UserDetail userDetail = new UserDetail
                    {
                        UserName = userRegistration.UserName,
                        UserDesignation = userRegistration.UserDesignation,
                        UserId = userRegistration.UserId
                    };

                    return new TDRResponse<UserDetail>(1, userDetail, null);
                }

                return new TDRResponse<UserDetail>(0, null, "please do check the password");
            }
            else
            {
                return new TDRResponse<UserDetail>(0, null,  "Not Found" );
            }

        }

        public async Task<TDRResponse<string>> UserRegigstraion(UserRegistration userRegistration)
        {
            bool _IsVerified = await TDRRepositery.VerifyUser(userRegistration.UserId);

            if (_IsVerified)
            {
                return new TDRResponse<string>(0, null, "user already exists.. try another");
            }

            bool _IsSaved = await TDRRepositery.AddUser(userRegistration);

            if (_IsSaved)
            {
                return new TDRResponse<string>(1, "registration successfull", null);
            }
            else
            {
                return new TDRResponse<string>(0, null,"Error Ocured while Registration, try again after some time");
            }
        }

        public async Task<TDRResponse<List<UserDetail>>> GetUserDetails()
        {
            List<UserRegistration> userRegistration = await TDRRepositery.GetUsers();

            if (userRegistration != null && userRegistration.Count > 0)
            {
                List<UserDetail> userDetails = new List<UserDetail>();

                foreach (var user in userRegistration)
                {
                    UserDetail userDetail = new UserDetail
                    {
                        UserName = user.UserName,
                        UserDesignation = user.UserDesignation,
                        UserId = user.UserId
                    };

                    userDetails.Add(userDetail);
                };
                return new TDRResponse<List<UserDetail>>(1, userDetails, null);
            }
            else
            {
                return new TDRResponse<List<UserDetail>>(0, null, "Not Found");
            }

        }

        public async Task<TDRResponse<UserDetail>> GetUserDetail(string user)
        {
            UserRegistration userRegistration = await TDRRepositery.GetUser(user);

            if (userRegistration != null)
            {
                UserDetail userDetail = new UserDetail
                {
                    UserName = userRegistration.UserName,
                    UserDesignation = userRegistration.UserDesignation,
                    UserId = userRegistration.UserId
                };

                return new TDRResponse<UserDetail>(1, userDetail, null);
            }
            else
            {
                return new TDRResponse<UserDetail>(0, null, "Not Found");
            }

        }

        public async Task<TDRResponse<Tasks>> GetTaskById(long taskId)
        {
            Tasks taskDesc = await TDRRepositery.GetTaskById(taskId);

            if (taskDesc != null)
            {
                
                return new TDRResponse<Tasks>(1, taskDesc, null);
            }
            else
            {
                return new TDRResponse<Tasks>(0, null, "Not Found");
            }

        }

        public async Task<TDRResponse<TaskCreated>> TaskDescription(TaskDesc taskDesc)
        {
            taskDesc.Date = DateTime.Now;

            taskDesc.Images = taskDesc.Img.Count().ToString();

            taskDesc.Accepted = "N";

            taskDesc.Rejected = "N";

            taskDesc.Solved = "N";

            taskDesc.UnSolved = "N";

            taskDesc.Resolved = "N";

            taskDesc.UnResolved = "N";

            taskDesc.Started = "N";

            taskDesc.ReIssued = "N";

            TaskCreated taskCreated = await TDRRepositery.SetTaskDesc(taskDesc);

            foreach (var imgId in taskDesc.Img)
            {
                int isSaved = await TDRRepositery.SaveImgTaskId(imgId, Int32.Parse(taskCreated.Id));
            }

            if (taskCreated != null)
            {
                return new TDRResponse<TaskCreated>(1, taskCreated, null);
            }
            else
            {
                return new TDRResponse<TaskCreated>(0, null, "server busy");
            }
        }

        public async Task<TDRResponse<List<TaskDesc>>> GetTaskDescriptions()
        {
            List<TaskDesc> taskDescs = await TDRRepositery.GetTaskDescs();

            

            if (taskDescs != null && taskDescs.Count > 0)
            {
                return new TDRResponse<List<TaskDesc>>(1, taskDescs, null);
            }
            else
            {
               return new TDRResponse<List<TaskDesc>>(0, null, "Not Found");
            }

        }

        public async Task<TDRResponse<List<TaskDesc>>> GetTaskDescriptionsByTaskBy(string taskBy)
        {
            List<TaskDesc> taskDescs = await TDRRepositery.GetTaskDescsBy(taskBy);

            if (taskDescs != null && taskDescs.Count >0)
            {
                return new TDRResponse<List<TaskDesc>>(1, taskDescs, null);
            }
            else
            {
                return new TDRResponse<List<TaskDesc>>(0, null, "Not Found");
            }
        }

        public async Task<TDRResponse<List<TaskDesc>>> GetTaskDescriptionsByTaskTo(string taskTo)
        {
            List<TaskDesc> taskDescs = await TDRRepositery.GetTaskDescsTo(taskTo);

            if (taskDescs != null && taskDescs.Count > 0)
            {
                return new TDRResponse<List<TaskDesc>>(1, taskDescs, null);
            }
            else
            {
                return new TDRResponse<List<TaskDesc>>(0, null, "Not Found");
            }
        }

        public async Task<TDRResponse<List<TaskDesc>>> GetTaskDescriptionByFiters(TaskDescFilter taskDescFilter)
        { 
            TDRResponse<List<TaskDesc>> tDRResponse  = new TDRResponse<List<TaskDesc>>();

            List<TaskDesc> taskDescs = await TDRRepositery.GetTaskDescs();

            if (taskDescs == null)
            {
                return new TDRResponse<List<TaskDesc>>(1, taskDescs, null);
            }
            if (taskDescs != null && taskDescs.Count > 0)
            {
                taskDescs = taskDescs.Where(t => taskDescFilter.TaskBy == null || taskDescFilter.TaskBy == t.TaskBy)
                     .Where(t => taskDescFilter.TaskTo == null || taskDescFilter.TaskTo == t.TaskTo)
                     .Where(t => taskDescFilter.Platform == null || taskDescFilter.Platform == t.Platform)
                     .Where(t => taskDescFilter.Accepted == null  || taskDescFilter.Accepted == t.Accepted)
                     .Where(t => taskDescFilter.Rejected == null || taskDescFilter.Rejected == t.Rejected)
                     .Where(t => taskDescFilter.Solved == null || taskDescFilter.Solved == t.Solved)
                     .Where(t => taskDescFilter.UnSolved == null || taskDescFilter.UnSolved == t.UnSolved)
                     .Where(t => taskDescFilter.Resolved == null || taskDescFilter.Resolved == t.Resolved)
                     .Where(t => taskDescFilter.UnReSolved == null || taskDescFilter.UnReSolved == t.UnResolved)
                     .Where(t => taskDescFilter.Date == null || taskDescFilter.Date == t.Date.ToString("dd/MM/yyyy"))
                     .ToList();

                if (taskDescs.Count > 0) 
                {
                    return new TDRResponse<List<TaskDesc>>(1, taskDescs, null);
                }

                return new TDRResponse<List<TaskDesc>>(1, null, null);

            }
            return new TDRResponse<List<TaskDesc>>(0, null, "Data not found");
        }

        public async Task<TDRResponse<string>> UpdateTaskDescriptions(TaskDesc taskDesc)
        {
            int isChanged = await TDRRepositery.UpdateTaskDescs(taskDesc);

            if (isChanged > 0)
            {
                return new TDRResponse<string>(1,"Saved SuccessFully", null);
            }
            return new TDRResponse<string>(0, null,"error");
        }

        public async Task<TDRResponse<int>> SaveImage(string Images, string type)
        {
            Image image = new Image
            {
                Img = Images,
                Type = type
            };

            int isChanged = await TDRRepositery.SaveImage(image);

            if (isChanged > 0)
            {
                return new TDRResponse<int>(1, isChanged, null);
            }
            return new TDRResponse<int>(0, 0, null);
        }

        public async Task<TDRResponse<List<Image>>> GetImage(int taskId)
        {
            List<Image> images = await TDRRepositery.GetImagesAsync(taskId);

            if (images?.Count > 0)
            {
                return new TDRResponse<List<Image>>(1, images, null);
            }
            return new TDRResponse<List<Image>>(0, null, "Not Found");
        }

        public async Task<TDRResponse<TaskCreated>> TaskDescriptionV101(Tasks taskDesc)
        {
            taskDesc.ImgCount = taskDesc.ImgArray.Count();

            

            TaskCreated taskCreated = await TDRRepositery.SetTaskDescV101(taskDesc);
           
            if (taskCreated != null)
            {
                foreach (var imgId in taskDesc.ImgArray)
                {
                    int isSaved = await TDRRepositery.SaveImgTaskId(Int32.Parse(imgId), Int32.Parse(taskCreated.Id));
                }

                WorkFlow workFlow = new WorkFlow
                {
                    TaskId = long.Parse(taskCreated.Id),

                    TaskType = taskDesc.TaskType,

                    TaskSub = taskDesc.TaskSub,

                    Status = taskDesc.TaskType.Equals("task", StringComparison.InvariantCultureIgnoreCase) ? "Assigned" : "Opened",

                    TaskDesc = taskDesc.TaskDesc,

                    Platform = taskDesc.Platform,

                    TaskById = taskDesc.TaskById,

                    TaskToId = taskDesc.TaskToId,

                    TaskByName = taskDesc.TaskByName,

                    TaskToName = taskDesc.TaskToName,

                    Priority = taskDesc.Priority,

                    ProjName = taskDesc.ProjName,

                    CreatedDt = DateTime.Now,

                    From = "Init",

                    To = "Open",

                    Flow = "F" ,

                    Role = "W",

                    DevName = taskDesc.DevName,

                    DevId = taskDesc.DevId,

                    Movedby = taskDesc.Movedby,

                    MovedbyId = taskDesc.MovedbyId

                };

                int isSved = await TDRRepositery.SetWorkFlow(workFlow);

                return new TDRResponse<TaskCreated>(1, taskCreated, null);
            }
            else
            {
                return new TDRResponse<TaskCreated>(0, null, "server busy");
            }


            throw new NotImplementedException();
        }
        public async Task<TDRResponse<List<Tasks>>> GetTaskDescriptionsV101()
        {
            List<Tasks> taskDescs = await TDRRepositery.GetTaskDescsV101();



            if (taskDescs != null && taskDescs.Count > 0)
            {
                return new TDRResponse<List<Tasks>>(1, taskDescs, null);
            }
            else
            {
                return new TDRResponse<List<Tasks>>(0, null, "Not Found");
            }

        }
        public async Task<TDRResponse<List<Tasks>>> GetTaskDescriptionByFitersV101(TasksFilter taskDescFilter,int pageIndex,int pageSize)
        {
            TDRResponse<List<Tasks>> tDRResponse = new TDRResponse<List<Tasks>>();

            List<Tasks> taskDescs = await TDRRepositery.GetTaskDescsV101();

            if (taskDescs == null)
            {
                return new TDRResponse<List<Tasks>>(1, taskDescs, null);
            }
            if (taskDescs != null && taskDescs.Count > 0)
            {
                taskDescs = taskDescs.OrderByDescending(t => t.ModifiedDt)
                    .Where(t => taskDescFilter.TaskType == null || taskDescFilter.TaskType == t.TaskType)
                     .Where(t => taskDescFilter.Status == null || taskDescFilter.Status == t.Status)
                     .Where(t => taskDescFilter.Platform == null || taskDescFilter.Platform == t.Platform)
                     .Where(t => taskDescFilter.DevStatus == null || taskDescFilter.DevStatus == t.DevStatus)
                     .Where(t => taskDescFilter.TestStatus == null || taskDescFilter.TestStatus == t.TestStatus)
                     .Where(t => taskDescFilter.DevId == null || taskDescFilter.DevId == t.DevId)
                     .Where(t => taskDescFilter.TaskById == null || taskDescFilter.TaskById == t.TaskById)
                     .Where(t => taskDescFilter.TaskToId == null ||(taskDescFilter.TaskToId == t.TaskToId || 
                     t.TaskToId == taskDescFilter?.Team))
                     .Where(t => taskDescFilter.Priority == null || taskDescFilter.Priority == t.Priority)
                     .Where(t => taskDescFilter.Started == null || taskDescFilter.Started == t.Started)
                     .Where(t => taskDescFilter.Reopened == null || taskDescFilter.Reopened == t.ReOpened)
                     .Where(t => taskDescFilter.CreatedDt == null || taskDescFilter.CreatedDt == t.CreatedDt.ToString("dd/MM/yyyy"))
                     .Where(t => taskDescFilter.ModifiedDt == null || taskDescFilter.ModifiedDt == t.ModifiedDt.ToString("dd/MM/yyyy"))
                     .Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

                if (taskDescs.Count > 0)
                {
                    return new TDRResponse<List<Tasks>>(1, taskDescs, null);
                }

                return new TDRResponse<List<Tasks>>(1, null, null);

            }
            return new TDRResponse<List<Tasks>>(0, null, "Data not found");
        }

        public async Task<TDRResponse<PaginationModel>> GetTasksByPaginationFitersV01(TasksFilter taskDescFilter,int pageIndex,int pageSize, string from)
        {
            TDRResponse<PaginationModel> tDRResponse = new TDRResponse<PaginationModel>();

            PaginationModel taskDescs = await TDRRepositery.GetTasksByPaginationFitersV01(taskDescFilter,pageIndex,pageSize,from);

            if (taskDescs == null)
            {
                return new TDRResponse<PaginationModel>(1, taskDescs, null);
            }else{
                return new TDRResponse<PaginationModel>(1, taskDescs, null);
            }
            
        }

        public async Task<TDRResponse<string>> UpdateTaskDescriptionsV101(Tasks taskDesc)
        {
           
            taskDesc.ModifiedDt = DateTime.Now;
            Console.Write(taskDesc.ModifiedDt);
            int isChanged = await TDRRepositery.UpdateTaskDescsV101(taskDesc);

            if (isChanged > 0)
            {
                WorkFlow workFlow = new WorkFlow
                {
                    TaskId = taskDesc.TaskId,

                    TaskType = taskDesc.TaskType,

                    TaskSub = taskDesc.TaskSub,

                    Status = taskDesc.Status,

                    DevStatus = taskDesc.DevStatus,

                    TestStatus = taskDesc.TestStatus,

                    TaskDesc = taskDesc.TaskDesc,

                    Platform = taskDesc.Platform,

                    TestRemark = taskDesc.TestRemark,

                    DevRemark = taskDesc.DevRemark,

                    TaskById = taskDesc.TaskById,

                    TaskToId = taskDesc.TaskToId,

                    TaskByName = taskDesc.TaskByName,

                    TaskToName = taskDesc.TaskToName,

                    Priority = taskDesc.Priority,

                    ProjName = taskDesc.ProjName,

                    CreatedDt = DateTime.Now,

                    From = taskDesc.From,

                    To = taskDesc.Status,

                    Flow =taskDesc.Flow  ,

                    Role = "W",

                    DevName = taskDesc.DevName,

                    DevId = taskDesc.DevId,

                    Movedby = taskDesc.Movedby,

                    MovedbyId = taskDesc.MovedbyId

                };

                int isSved = await TDRRepositery.SetWorkFlow(workFlow);

                return new TDRResponse<string>(1, "Saved SuccessFully", null);
            }
            return new TDRResponse<string>(0, null, "error");
        }


        public async Task<TDRResponse<CountModel>> GetTaskCountV01()
        {
            CountModel countModel = await TDRRepositery.GetTaskCountV01();

            if (countModel != null)
            {
                return new TDRResponse<CountModel>(1, countModel, null);
            }

            return new TDRResponse<CountModel>(1, null, "Data not found");
        }

        public async Task<TDRResponse<List<WorkFlow>>> GetWorkFlowById(int id)
        {
            List<WorkFlow> workFlow = await TDRRepositery.GetWorkFlowAsync(id);

            if (workFlow != null)
            {
                return new TDRResponse<List<WorkFlow>>(1, workFlow, null);
            }
            return new TDRResponse<List<WorkFlow>>(1, null, "Data not found");
        }

        public async Task<TDRResponse<List<WorkFlow>>> GetWorkFlows()
        {
            List<WorkFlow> workFlows = await TDRRepositery.GetWorkFlowAsync();

            if (workFlows.Count > 0)
            {
                return new TDRResponse<List<WorkFlow>>(1, workFlows, null);
            }
            return new TDRResponse<List<WorkFlow>>(1, null, "Data not found");

        }

        public async Task<TDRResponse<List<WorkFlow>>> GetWorkFlowsByPage(int pageIndex, int pageSize)
        {
            List<WorkFlow> workFlows = await TDRRepositery.GetWorkFlowAsync();

            if (workFlows.Count > 0)
            {
                workFlows = workFlows.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                if(workFlows.Count > 0)
                    return new TDRResponse<List<WorkFlow>>(1, workFlows, null);
            }
            return new TDRResponse<List<WorkFlow>>(1, null, "Data not found");
        }
    }
}
