﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdr.jay.api.Entities;
using tdr.jay.api.Repository;

namespace tdr.jay.api.Bussiness
{
    public class ChatHub : Hub
    {
        public TdrDBContext TdrDBontext { get; }

        public ChatHub(TdrDBContext tdrDBontext)
        {
            TdrDBontext = tdrDBontext;
        }
       
        public async Task SendMessage(Message message)
        {
            WorkFlow workFlow = new WorkFlow
            {
                TaskByName = message.UserName,

                TaskId = long.Parse(message.TaskId),

                TaskDesc = message.Msg,

                TaskById = message.UserId,

                Role = "M",

                CreatedDt = DateTime.Now
            };
            Console.WriteLine("entered");
            try
            {
                Console.WriteLine("saveing to db" + message.Msg);

                await TdrDBontext.workFlows.AddAsync(workFlow);

                int i =  await TdrDBontext.SaveChangesAsync();

                Console.WriteLine("saveing to db" + i);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            Console.WriteLine("3"+message.TaskId);
            try
            {
                await Clients.Group(message.TaskId.ToString()).SendAsync("Send", workFlow);
            }
            catch
            {
                Console.WriteLine("error in client" + message.TaskId);
            }
           

            Console.WriteLine("1", message.TaskId);
        }

        public async Task JoinGroup(string taskId)
        {
            if(taskId != "-999999999"){
                if(!TdrDBontext.tasks.Where(c => c.TaskId.ToString() == taskId).Any())
                {
                    throw new System.Exception("Task id not valid");
                }
            }
            

            await Groups.AddToGroupAsync(Context.ConnectionId, taskId);
            await Clients.Group(taskId).SendAsync("JoinGroup", taskId);
            Console.WriteLine("3", Context.ConnectionId, taskId);

            try
            {
                 var ee = long.Parse(taskId);
                 Console.WriteLine(ee);
                Console.WriteLine("triggering History 23 "+ taskId);
                var history = TdrDBontext.workFlows.Where(a => a.TaskId == long.Parse(taskId) ).ToList();

                Console.WriteLine("triggering History"+ history);

                await Clients.Client(Context.ConnectionId).SendAsync("History", history);
            }
            catch (Exception ex)
            {
                Console.WriteLine("historyError" + ex.Message);
            }


            
        }

        public async Task LeaveGroup(string groupName)
        {
            Console.WriteLine("1" + groupName);
            try
            {
                await Clients.Group(groupName).SendAsync("LeaveGroup", groupName);

                await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
         
        }

    }
}
