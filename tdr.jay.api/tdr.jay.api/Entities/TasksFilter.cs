﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace tdr.jay.api.Entities
{
    public class TasksFilter
    {
        public string TaskType { get; set; }
        public string Status { get; set; }
        public string TaskById { get; set; }
        public string TaskToId { get; set; }
        public string Platform { get; set; }
        public string Priority { get; set; }
        public string CreatedDt { get; set; }  
        public string ModifiedDt { get; set; }
        public string DevStatus { get; set; }
        public string TestStatus { get; set; }
        public string DevId { get; set; }
        public string Started { get; set; }
        public string Reopened { get; set; }
        public string ProjName { get; set; }
        public string Team { get; set; }
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string from { get; set; }

    }
}
