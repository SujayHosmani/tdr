﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdr.jay.api.Entities
{
    public class TaskDescFilter
    {

        public string TaskBy { get; set; }
        public string TaskTo { get; set; }
        public string Platform { get; set; }
        public string Accepted { get; set; }
        public string Rejected { get; set; }
        public string Solved { get; set; }
        public string UnSolved { get; set; }
        public string Resolved { get; set; }
        public string UnReSolved { get; set; }
        public string Date { get; set; }
    }
}
