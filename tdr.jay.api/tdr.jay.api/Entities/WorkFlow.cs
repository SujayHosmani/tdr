﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdr.jay.api.Entities
{
    public class WorkFlow
    {
        public long Id { get; set; }
        public long TaskId { get; set; }
        public string TaskType { get; set; }
        public string TaskSub { get; set; }
        public string Status { get; set; }
        public string DevStatus { get; set; }
        public string TestStatus { get; set; }
        public string TaskDesc { get; set; }
        public string Platform { get; set; }
        public string TestRemark { get; set; }
        public string DevRemark { get; set; }
        public string TaskById { get; set; }
        public string TaskToId { get; set; }
        public string TaskByName { get; set; }
        public string TaskToName { get; set; }
        public string Priority { get; set; }
        public string ProjName { get; set; }
        public DateTime CreatedDt { get; set; }
        public string DevName { get; set; }
        public string DevId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Flow { get; set; }
        public string Role { get; set; }
        public string Movedby { get; set; }
        public string MovedbyId { get; set; }
    }
}
