using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdr.jay.api.Entities
{
    public class PaginationModel
    {
        public List<Tasks> Open { get; set; }
        public List<Tasks> Progress { get; set; }
        public List<Tasks> Resolved { get; set; }
        public List<Tasks> Closed { get; set; }
        public List<Tasks> Reopened { get; set; }

        public int OpenCount { get; set; }
        public int ProgressCount { get; set; }
        public int ResolvedCount { get; set; }
        public int ClosedCount { get; set; }
        public int ReopenedCount { get; set; }
        public int TotalCount { get; set; }

    }
}
