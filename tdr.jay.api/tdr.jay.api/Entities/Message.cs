﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdr.jay.api.Entities
{
    public class Message
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string TaskId { get; set; }
        public string Msg { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; }
        
    }
}
