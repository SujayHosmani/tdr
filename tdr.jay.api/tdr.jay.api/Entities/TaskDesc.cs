﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace tdr.jay.api.Entities
{
    public class TaskDesc
    {
		public string TaskTitle { get; set; }
		[Required]
		public string TaskDescription { get; set; }
		public string Images { get; set; }
		[Required]
		public string Platform { get; set; }
		[Required]
		public string TaskBy { get; set; }
		[Required]
		public string TaskTo { get; set; }
		[JsonIgnore]
		public DateTime Date { get; set; }
		public string Priority { get; set; }
		public string Accepted { get; set; }
		public string Rejected { get; set; }
		public string Solved { get; set; }
		public string DevRemark { get; set; }
		public string TesterRemark { get; set; }
		public string Started { get; set; }
		public string Resolved { get; set; }
		public long ReIssueId { get; set; }
		public string ReIssued { get; set; }
		public long TaskId { get; set; }
		[Required]
		public string From { get; set; }
		[Required]
		public string To { get; set; }
		public string UnSolved { get; set; }
		public string UnResolved { get; set; }
		public  string DevName { get; set; }
		public string DevId { get; set; }
		public string ProjectName { get; set; }
		[NotMapped]
		public  List<int> Img{ get; set; }
	}
}
