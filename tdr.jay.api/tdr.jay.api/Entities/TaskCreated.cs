﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdr.jay.api.Entities
{
    public class TaskCreated
    {
        public  string Id { get; set; }
        public string CreatedDt { get; set; }
        public string CreatedUser { get; set; }

    }
}
