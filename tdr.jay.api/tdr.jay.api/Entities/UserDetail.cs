﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdr.jay.api.Entities
{
    public class UserDetail
    {
        public string UserName { get; set; }
        public string UserDesignation { get; set; }
        public string UserId { get; set; }
    }
}
