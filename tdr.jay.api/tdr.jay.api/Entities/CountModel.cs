﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdr.jay.api.Entities
{
    public class CountModel
    {
        public int Open { get; set; }
        public int Progress { get; set; }
        public int Resolved { get; set; }
        public int Closed { get; set; }
        public int Reopened { get; set; }
        public int Total { get; set; }


    }
}
