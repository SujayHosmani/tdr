﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using tdr.jay.api.Bussiness;
using tdr.jay.api.Bussiness.Response;
using tdr.jay.api.Entities;
using tdr.jay.api.Repository.Contracts;

namespace tdr.jay.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TDRController : Controller
    {
        public TDRController(ITDRService tDRService,
            IHubContext<ChatHub> hub)
        {
            TDRService = tDRService;
            Hub = hub;
            //hu333
        }

        public ITDRService TDRService { get; }
        public IHubContext<ChatHub> Hub { get; }

        
        public async Task SendToAll(string msg, string name)
        {
            await Hub.Clients.All.SendAsync("SendToAll", msg, name);
        }

        [Route("login")]
        [HttpPost]
        public async Task<ActionResult<TDRResponse<UserDetail>>> VerifyUser(Login login)
        {
            return await TDRService.VerifyUser(login);
        }

        [Route("Registration")]
        [HttpPost]
        public async Task<ActionResult<TDRResponse<string>>> UserRegistration(UserRegistration userRegistration)
        {
            return await TDRService.UserRegigstraion(userRegistration);
        }

        [Route("users")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<List<UserDetail>>>> GetUsers()
        {
            return await TDRService.GetUserDetails();
        }

        [Route("user/{userid}")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<UserDetail>>> GetUser(string userid)
        {
            return await TDRService.GetUserDetail(userid);
        }

        [Route("task/{taskid}")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<Tasks>>> GetTaskById(long taskid)
        {
            return await TDRService.GetTaskById(taskid);
        }

        [Route("taskdesc")]
        [HttpPost]
        public async Task<ActionResult<TDRResponse<TaskCreated>>> TaskDescription(TaskDesc taskDesc)
        {
            return await TDRService.TaskDescription(taskDesc);
        }

        [Route("gettaskdesc")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<List<TaskDesc>>>> GetTaskDescriptions()
        {
            return await TDRService.GetTaskDescriptions();
        }

        [Route("gettaskdescby/{taskby}")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<List<TaskDesc>>>> GetTaskDescriptionsBy(string taskby)
        {
            return await TDRService.GetTaskDescriptionsByTaskBy(taskby);
        }

        [Route("gettaskdescto/{taskto}")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<List<TaskDesc>>>> GetTaskDescriptionsTo(string taskto)
        {                                                                                                    
            return await TDRService.GetTaskDescriptionsByTaskTo(taskto);
        }
        [Route("taskdesc/filter")]
        [HttpPost]
        public async Task<ActionResult<TDRResponse<List<TaskDesc>>>> GetTaskDescriptionByFiters(TaskDescFilter taskDescFilter)
        {
            return await TDRService.GetTaskDescriptionByFiters(taskDescFilter);
        }
        [Route("updatetask")]
        [HttpPost]
        public async Task<ActionResult<TDRResponse<string>>> UpdateTaskDescriptions(TaskDesc taskDesc)
        {
            return await TDRService.UpdateTaskDescriptions(taskDesc);
        }

        [Route("saveimage")]
        [HttpPost]
        public async Task<ActionResult<TDRResponse<int>>> SaveImage(Image images)
        {
            return await TDRService.SaveImage(images.Img, images.Type);
        }

        [Route("getimage/{taskid}")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<List<Image>>>> GetImage(int taskid)
        {
            return await TDRService.GetImage(taskid);
        }

        [Route("taskdescV01")]
        [HttpPost]
        public async Task<ActionResult<TDRResponse<TaskCreated>>> TaskDescriptionV01(Tasks taskDesc)
        {
            return await TDRService.TaskDescriptionV101(taskDesc);
        }

        [Route("gettaskdescV01")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<List<Tasks>>>> GetTaskDescriptionsV01()
        {
            return await TDRService.GetTaskDescriptionsV101();
        }
        [Route("taskdesc/filterV01")]
        [HttpPost]
        public async Task<ActionResult<TDRResponse<List<Tasks>>>> GetTaskDescriptionByFitersV01(TasksFilter taskDescFilter)
        {
            return await TDRService.GetTaskDescriptionByFitersV101(taskDescFilter, taskDescFilter.pageNumber, taskDescFilter.pageSize);
        }

        [Route("taskdesc/pagination/filterV01")]
        [HttpPost]
        public async Task<ActionResult<TDRResponse<PaginationModel>>> GetTasksByPaginationFitersV01(TasksFilter taskDescFilter)
        {
            return await TDRService.GetTasksByPaginationFitersV01(taskDescFilter, taskDescFilter.pageNumber, taskDescFilter.pageSize, taskDescFilter.from);
        }


        [Route("updatetaskV01")]
        [HttpPost]
        public async Task<ActionResult<TDRResponse<string>>> UpdateTaskDescriptionsV01(Tasks taskDesc)
        {
            return await TDRService.UpdateTaskDescriptionsV101(taskDesc);
        }

        [Route("getCountV01")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<CountModel>>> GetTaskCountV01()
        {
            return await TDRService.GetTaskCountV01();
        }

        [Route("getworkflow/{id}")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<List<WorkFlow>>>> GetWorkFlow(int id)
        {
            return await TDRService.GetWorkFlowById(id);
        }

        [Route("getworkflows")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<List<WorkFlow>>>> GetWorkFlows()
        {
            return await TDRService.GetWorkFlows();
        }

        [Route("getworkflowbypage/{number}/{size}")]
        [HttpGet]
        public async Task<ActionResult<TDRResponse<List<WorkFlow>>>> GetWorkFlowbypage(int number, int size)
        {
            return await TDRService.GetWorkFlowsByPage(number, size);
        }

    }
}